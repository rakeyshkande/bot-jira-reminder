# Introduction

This bot answer a really simple need in our small team (~6 people full time) : have a daily reminder of who is doing what.

This bot will simply crawl through the provided list of users, get the opened tasks in Jira and display a summary in the channel of your choice.

![alt text][screenshot_1]

## Installation

* Simply clone the project and `npm install`
* Then, go to config.json and modify the settings
* To launch `npm start`

## Internationalization

There is not a lot of text to translate, but you can change the current language in config.json (you will need to provide the translation in locales/yourlanguage.json).

[screenshot_1]: http://i.imgur.com/FhM7JaB.png "Example Screenshot"
