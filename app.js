const config = require('./config.json');
const _ = require('lodash');
const schedule = require('node-schedule');
const JiraApi = require('jira-client') ;
const i18n = require("i18n");

// Slack API
const WebClient = require('@slack/client').WebClient;
const web = new WebClient(config.slackToken);

// i18n configuration
i18n.configure({
  directory: './locales'
});
i18n.setLocale(config.lang);

// Jira configuration
const jira = new JiraApi({
  protocol: 'https',
  host: config.jiraConfig.host,
  username: config.jiraConfig.username,
  password: config.jiraConfig.password,
  apiVersion: '2',
  strictSSL: true
});

/***
 * The core method, will get the issues from Jira and post a message on Slack
 * @constructor
 */
let DisplayActiveTasks = function () {
  let promises = [];
  let users = config.users;

  // For each user we get their open issues
  // We put the call in a promises array
  users.forEach(user => {
    promises.push(
      jira.getUsersIssues(user.jiraID, true)
        .then(res => {
          user.issues = res.issues;
        })
        .catch(err => {
          console.error(err);
        }))
  });

  // Once all our promises have resolved, we prepare and send the Slack message
  // For each user we create an attachment, issues are separated by line endings
  // https://api.slack.com/docs/message-attachments
  Promise.all(promises)
    .then(() => {
      let message = i18n.__("DailyRecapTitle");
      let attachments = [];
      users.forEach(user => {
        let attachment = {};
        attachment.title = user.slackID.charAt(0).toUpperCase() + user.slackID.slice(1) + ' ' + i18n.__("IsWorkingOn");
        attachment.fallback = i18n.__("DailyRecapFallback");
        attachment.mrkdwn_in = [ "text", "pretext" ];
        attachment.color = user.issues.length == 0 ? "#FF0000" : "#0CD100";

        // We build the attachment text
        if (user.issues.length == 0) {
          attachment.text = i18n.__("NothingAlert");
        } else {
          attachment.text = _.map(user.issues, (issue) => {
            let issueText = issue.fields.summary;
            if (issue.fields.timeoriginalestimate) {
              let percent = Math.trunc(issue.fields.aggregatetimespent / issue.fields.timeoriginalestimate * 100);
              issueText += ' - ' + percent + '%';
              if (percent <= 100) {
                issueText += ' :+1:';
              } else if (percent > 100 && percent <= 200) {
                issueText += ' :cold_sweat:';
              } else {
                issueText += ' :scream:';
              }
            }
            return issueText;
          }).join("\n");
        }
        attachments.push(attachment);
      });

      // We send the message using Slack's web API
      web.chat.postMessage(config.dailyMessageChannel, message, {
        attachments: attachments,
        as_user: true
      })
        .then((res) => console.log(res));
    });
}

// We schedule
config.dailyMessageScheduleRules.forEach(rule => {
  let scheduleRule = new schedule.RecurrenceRule();
  scheduleRule.dayOfWeek = rule.dayOfWeek;
  scheduleRule.hour = rule.hour;
  scheduleRule.minute = rule.minute;
  schedule.scheduleJob(rule, DisplayActiveTasks);
});